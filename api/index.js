const bull = require('bull');

const config = { redis: { host: 'localhost', port: 6379 } };

const queueView = bull("Curso:view", config);
const queueCreate = bull("curso:create", config);
const queueDelete = bull("curso:delete", config);
const queueUpdate = bull("curso:update", config);
const queueGetById = bull("curso:getbyid", config);


const Create = async ({ name, edad, color }) => {
    try {
        const job = await queueCreate.add({ name, edad, color });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.error(error)
    }
};

const Delete = async ({ id }) => {
    try {
        const job = await queueDelete.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.error(error)
    }
};

const Update = async ({ id, name, edad, color }) => {
    try {
        const job = await queueUpdate.add({ name, edad, color, id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };
    } catch (error) {
        console.error(error)
    }
};

const GetById = async ({ id }) => {
    try {
        const job = await queueGetById.add({ id });
        const { statusCode, data, message } = await job.finished();
        return { statusCode, data, message };

    } catch (error) {
        console.error(error)
    }
};

const View = async ({ }) => {
    try {
        // console.log('api View')
        const job = await queueView.add({});
        const { statusCode, data, message } = await job.finished();
        // console.log('Response View',{ statusCode, data, message })
        return { statusCode, data, message };
    } catch (error) {
        console.error(error)
    }
};


const main = async () => {
    // await Create({name:'Knut', edad:37, color:'rojo'});
    // await Create({name:'Knut2', edad:38, color:'verde'});
    // await Delete({ id: 25 });
    // await Update({ id: 27, name: 'New Knut', edad: 22, color: 'azul' });
    // await GetById({id:26});
    // await View({});
}

// main();

module.exports = {
    Create,
    Delete,
    Update,
    GetById,
    View
}