const { io } = require('socket.io-client');

// const socket = io("http://localhost", { port:3000, transports: ['websocket'] });
const socket = io("http://localhost", { port: 3000 });

const main = () => {
    try {
        setTimeout(() => {
            console.log('ID: ', socket.id);
        }, 500);
        //EVENTOS
        //Res Create
        socket.on('res:microservice:create', ({ statusCode, data, message }) => {
            console.log('res:microservice:create', { statusCode, data, message })
        })
        //Res Delete
        socket.on('res:microservice:delete', ({ statusCode, data, message }) => {
            console.log('res:microservice:delete', { statusCode, data, message })
        })
        //Res Update
        socket.on('res:microservice:update', ({ statusCode, data, message }) => {
            console.log('res:microservice:update', { statusCode, data, message })
        })
        //Res Getbyid
        socket.on('res:microservice:getbyid', ({ statusCode, data, message }) => {
            console.log('res:microservice:getbyid', { statusCode, data, message })
        })
        //Res View
        socket.on('res:microservice:view', ({ statusCode, data, message }) => {
            console.log('res:microservice:view', { statusCode, data, message })
        })

        // setInterval(() => {
            // }, 500);
            setTimeout(() => {
                // socket.emit('req:microservice:create', ({ name:'Pedro', edad:65, color:'verde' }))
                socket.emit('req:microservice:update', ({ id:29, name:'Actualizado', edad:99, color:'naranja' }))
                socket.emit('req:microservice:delete', ({id:24}))
                socket.emit('req:microservice:view', ({}))

        }, 1000);
    } catch (error) {
        console.error(error)
    }
}
main()