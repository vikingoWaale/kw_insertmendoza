const express = require('express');
const http = require('http');

const app = express();

const server = http.createServer(app);

const{ Server } = require('socket.io');

const io = new Server(server);

const api = require('../api');

server.listen(80,()=>{
    console.log("Server listen")
    io.on('connection', socket =>{
        console.log('New connection', socket.id)
        //Metodos de consulta
        socket.on('req:microservice:create', async ({ name, edad, color })=>{
            try {
                console.log('Emitiendo create')
                const { statusCode, data, message } = await api.Create({ name, edad, color });
                return io.to(socket.id).emit('res:microservice:create',{ statusCode, data, message });                
            } catch (error) {
                console.error(error)
            }
        })
        socket.on('req:microservice:delete', async ({ id })=>{
            try {
                console.log('Emitiendo delete')
                const { statusCode, data, message } = await api.Delete({ id });
                return io.to(socket.id).emit('res:microservice:delete',{ statusCode, data, message });                
            } catch (error) {
                console.error(error)
            }
        })
        socket.on('req:microservice:update', async ({ id, name, edad, color })=>{
            try {
                console.log('Emitiendo update')
                const { statusCode, data, message } = await api.Update({ id, name, edad, color });
                return io.to(socket.id).emit('res:microservice:update',{ statusCode, data, message });                
            } catch (error) {
                console.error(error)
            }
        })
        socket.on('req:microservice:getbyid', async ({ id })=>{
            try {
                console.log('Emitiendo getbyid')
                const { statusCode, data, message } = await api.GetById({id});
                return io.to(socket.id).emit('res:microservice:getbyid',{ statusCode, data, message });                
            } catch (error) {
                console.error(error)
            }
        })
        socket.on('req:microservice:view', async ({})=>{
            try {
                console.log('Emitiendo view')
                // const { statusCode, data, message } = await api.View();
                const responseView = await api.View({});
                // return console.log('PRUEBA',{statusCode, data, message})
                // return console.log('PRUEBA',prueba)
                return io.to(socket.id).emit('res:microservice:view',responseView);
            } catch (error) {
                console.error(error)
            }
        })
    })
})

