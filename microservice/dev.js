const micro = require('./src');

const main = async ()=>{
    try {
        console.log('main de dev.js')
        const db = await micro.SyncDB();
        if(db.statusCode !== 200) throw db.message
        await micro.run();
        
    } catch (error) {
        console.error(error);
    }
}
main();