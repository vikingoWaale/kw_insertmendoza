const { application_name } = require('pg/lib/defaults');
const { Adapter } = require('./src/Adapters');

/*
Flujo
Api
Adapter
Service
Controller
DB
Respuesta
Controller
Servicio
Adapter
Api


*/


const main = async () => {
    try {
        console.log('Principal index.js')
        const result = await Adapter({ id: 2 })
        if (result.statusCode !== 200) throw (result.message)
        console.log("tu data es ", result.data)
    } catch (error) {
        console.error(error)
    }
}
main();
