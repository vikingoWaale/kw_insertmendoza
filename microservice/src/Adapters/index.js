const bull = require('bull');
const { redis } = require('../settings');

const config = { redis: { host: redis.host, port: redis.port } };

const queueView = bull("Curso:view", config);
const queueCreate = bull("curso:create", config);
const queueDelete = bull("curso:delete", config);
const queueUpdate = bull("curso:update", config);
const queueGetById = bull("curso:getbyid", config);

module.exports = {
    queueCreate,
    queueDelete,
    queueUpdate,
    queueGetById,
    queueView,
}

