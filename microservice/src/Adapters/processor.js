const Services = require('../Services');
const { InternalError } = require('../settings');
const {
    queueCreate,
    queueDelete,
    queueUpdate,
    queueGetById,
    queueView } = require('.')

const Create = async (job, done) => {
    try {
        const { name, edad, color } = job.data
        let { statusCode, data, message } = await Services.Create({ name, edad, color });
        done(null, { statusCode, data, message });
    } catch (error) {
        console.error({ step: 'Adapter Create', error: error.toString() });
        done(null, { statusCode: 500, message: InternalError });
    }
}

const Delete = async (job, done) => {
    try {
        console.log('Adapter Processor Delete');
        const { id } = job.data
        let { statusCode, data, message } = await Services.Delete({ id });
        // console.log('message',message)

        // let ver = await Services.Delete({ id });
        // console.log('Services.Delete',ver)
        done(null, { statusCode, data, message });
    } catch (error) {
        console.error({ step: 'Adapter Delete', error: error.toString() });
        done(null, { statusCode: 500, message: InternalError });
    }
}

const Update = async (job, done) => {
    try {
        const { id, name, edad, color } = job.data
        let { statusCode, data, message } = await Services.Update({ id, name, edad, color });
        console.log('Linea 13')
        done(null, { statusCode, data, message });
    } catch (error) {
        console.error({ step: 'adapter Adapter', error: error.toString() });
        done(null, { statusCode: 500, message: InternalError });
    }
}

const GetById = async (job, done) => {
    try {
        const { id } = job.data
        let { statusCode, data, message } = await Services.GetById({ id });
        console.log('Linea 13')
        done(null, { statusCode, data, message });
    } catch (error) {
        console.error({ step: 'adapter Adapter', error: error.toString() });
        done(null, { statusCode: 500, message: InternalError });
    }
}

const View = async (job, done) => {
    try {
        const { } = job.data
        console.log('View job.id',job.id)
        let { statusCode, data, message } = await Services.View({});
        done(null, { statusCode, data, message });
    } catch (error) {
        console.error({ step: 'adapter Adapter', error: error.toString() });
        done(null, { statusCode: 500, message: InternalError });
    }
}

const run = async () => {
    try {
        console.log('worker run')
        queueCreate.process(Create);
        queueDelete.process(Delete);
        queueUpdate.process(Update);
        queueGetById.process(GetById);
        queueView.process(View);
    } catch (error) {
        console.error(error)
    }
}
module.exports = {
    Create,
    Delete,
    Update,
    GetById,
    View,
    run
}