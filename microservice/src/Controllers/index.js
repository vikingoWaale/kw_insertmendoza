const { Model } = require('../Models');


async function Create({ name, edad, color}) {
    try {
        console.log('funcion Create')
        const instance = await Model.create({ name, edad, color},{logging:false});
        return { statusCode: 200, data: instance.toJSON() }
    } catch (error) {
        console.error({step: 'Controller Create', error: error.toString()});
        return { statusCode: 500, message: error.toString()}
    }
}

async function Delete(where = {}) {
    try {
        console.log('funcion Delete')
        await Model.destroy({where},{logging:false});
        return { statusCode: 200, data: `Ok`}
    } catch (error) {
        console.error({step: 'Controller Delete', error: error.toString()});
        return { statusCode: 500, message: error.toString()}
    }
};
async function Update({ id, name, edad, color}) {
     try {
        console.log('funcion Update');
        await Model.update({ name, edad, color }, { where: { id } },{logging:false});
        return { statusCode: 200, data: 'Actualizado'}
    } catch (error) {
        console.error({step: 'Controller Update', error: error.toString()});
        return { statusCode: 500, message: error.toString()}
    }
};
async function GetById({id}) {
     try {
         console.log('Controller GetById',id)
        // const instance = await Model.findOne({where:id},{logging:false});
        const instance = await Model.findByPk(id,{logging:false});
        
        if(instance) return { statusCode: 200, data: instance.toJSON()};
        else return { statusCode: 400, message: 'No exite el Usuario.'}
    } catch (error) {
        console.error({step: 'Controller GetById', error: error.toString()});
        return { statusCode: 500, message: error.toString()}
    }
};

async function View( where = {}) {
    try {
       console.log('funcion GetAll')
       const instances = await Model.findAll({where},{logging:false});
        return { statusCode: 200, data: instances}
   } catch (error) {
       console.error({step: 'Controller GetAll', error: error.toString()});
       return { statusCode: 500, message: error.toString()}
   }
};


module.exports = {
    Create,
    Delete,
    Update,
    GetById,
    View
}