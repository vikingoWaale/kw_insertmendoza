const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize')

const Model =  sequelize.define('Curso',{
    name:{
        type: DataTypes.STRING
    },
    edad:{
        type: DataTypes.INTEGER
    },
    color:{
        type: DataTypes.STRING
    }
});

async function SyncDB(){
    try {
        console.log('Inicializa DB');
        const model = await Model.sync({logging:false})
        console.log('DB inicializada');
        return { statusCode: 200, data:'Ok' }
    } catch (error) {
        console.error(error)
        return { statusCode: 500, message: error.toString()}
    }
}  
    

module.exports = {
    SyncDB,
    Model
}