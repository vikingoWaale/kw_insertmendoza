const Controller = require('../Controllers');
const { InternalError } = require('../settings');

async function Create({ name, edad, color }) {
    try {

        const { statusCode, data, message } = await Controller.Create({ name, edad, color })
        return { statusCode, data, message }
    } catch (error) {
        console.error({ step: 'service Create', error: error.toString() })
        return { statuCode: 500, message: error.toString() }
    }
}

async function Delete({ id }) {
    try {
        console.log('Service Delete');
        const elemento = await Controller.GetById({ id });
        if(elemento.statusCode !== 200){
            // console.log('Entra IF')
            const response = {
                400:{ statusCode: 400, message: "No existe el usurio a eliminar"},
                500:{ statusCode: 500, message: InternalError},
            }
            // console.log('response[elemento.statusCode]',response)
            return response[elemento.statusCode]
            || 
            {statusCode: elemento.statusCode, message: elemento.message}
        }
        const elementDel = await Controller.Delete({ id });
        // console.log('elementDel+++',{statusCode:200, data:elemento.data, message:`Ok`})
        if(elementDel.statusCode === 200){
            // console.log('return Service Delete', { statusCode:200, data:{id:elemento.data.id,name:elemento.data.name }, message:`Ok` })
            return { message:`Elimnado`, statusCode:200, data:{id:elemento.data.id,name:elemento.data.name },  }
        }
        return { statusCode: 400, message: InternalError }

    } catch (error) {
        console.error({ step: 'service Delete', error: error.toString() })
        return { statuCode: 500, message: error.toString() }
    }
}

async function Update({ id, name, edad, color}) {
    try {

        const { statusCode, data, message } = await Controller.Update({ name, edad, color, id })
        return { statusCode, data, message }
    } catch (error) {
        console.error({ step: 'service Update', error: error.toString() })
        return { statuCode: 500, message: error.toString() }
    }
}

async function GetById({ id }) {
    try {

        const { statusCode, data, message } = await Controller.GetById({ id })
        return { statusCode, data, message }
    } catch (error) {
        console.error({ step: 'service GetById', error: error.toString() })
        return { statuCode: 500, message: error.toString() }
    }
}

async function GetAll() {
    try {

        const { statusCode, data, message } = await Controller.GetAll()
        return { statusCode, data, message }
    } catch (error) {
        console.error({ step: 'service GetAll', error: error.toString() })
        return { statuCode: 500, message: error.toString() }
    }
}

async function View() {
    try {

        const { statusCode, data, message } = await Controller.View()
        return { statusCode, data, message }
    } catch (error) {
        console.error({ step: 'service View', error: error.toString() })
        return { statuCode: 500, message: error.toString() }
    }
}


module.exports = {
    Create,
    Delete,
    Update,
    GetById,
    GetAll,
    View,
}
