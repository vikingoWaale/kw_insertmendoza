const { SyncDB } = require('./Models');
const { run } = require('./Adapters/processor');

console.log('src/index.js')

module.exports = {
    SyncDB,
    run
}