const dotenv = require('dotenv');
const { Sequelize } = require('sequelize');



dotenv.config();

const redis = {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
}

const InternalError = 'No podemos procesar tu solicitud, en estos momentos.'

const squelizeConfig = {
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT,
    database:process.env.POSTGRES_DB,
    username:process.env.POSTGRES_USER,
    password:process.env.POSTGRES_PASSWORD,
    dialect: 'postgres'
};
// console.log('Variables')
// console.log(squelizeConfig)

const sequelize = new Sequelize(squelizeConfig);

module.exports = {
    redis,
    InternalError,
    sequelize
}