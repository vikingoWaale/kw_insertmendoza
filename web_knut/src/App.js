import { useEffect, useState } from 'react';
const { io } = require('socket.io-client');


function App() {
  const [id, setId] = useState();
  const [data, setData] = useState([]);

  useEffect(() => {
    const socket = io("http://localhost", {
      //cors solutions
      transports: ['websocket'],
      jsonp: false
    });
    console.log('useEffect');
    setTimeout(() => setId(socket.id), 200);
    socket.on('res:microservice:create', ({ statusCode, data, message }) => {
      console.log('res:microservice:create', { statusCode, data, message })
    })
    //Res Delete
    socket.on('res:microservice:delete', ({ statusCode, data, message }) => {
      console.log('res:microservice:delete', { statusCode, data, message })
    })
    //Res Update
    socket.on('res:microservice:update', ({ statusCode, data, message }) => {
      console.log('res:microservice:update', { statusCode, data, message })
    })
    //Res Getbyid
    socket.on('res:microservice:getbyid', ({ statusCode, data, message }) => {
      console.log('res:microservice:getbyid', { statusCode, data, message })
    })
    //Res View
    socket.on('res:microservice:view', ({ statusCode, data, message }) => {
      console.log('res:microservice:view', { statusCode, data, message })
      if (statusCode === 200) {
        setData(data)
      }
    })
    //Consulta api
    socket.emit('req:microservice:view', ({}))
  }, [])
  return (
    <div>
      {id ? `Conectado id ${id}` : `No conectado`}
      {data?.map(d => <p key= {p.id}>name: {d.name}, edad: {d.edad}, color: {d.color}</p>)}
    </div>
  );
}

export default App;
